using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace Mirror.Examples.MultipleAdditiveScenes
{
    public class PlayerScore : NetworkBehaviour
    {
        [SyncVar]
        public string playerName;

        [SyncVar]
        public int playerNumber;
       
        [SyncVar]
        public int scoreIndex;

        [SyncVar]
        public int matchIndex;

        [SyncVar]
        public uint score;

        public int clientMatchIndex = -1;

        // runs like update
        void OnGUI()
        {
            if (!isServerOnly && !isLocalPlayer && clientMatchIndex < 0)            
                clientMatchIndex = NetworkClient.connection.identity.GetComponent<PlayerScore>().matchIndex;
               
            if (isLocalPlayer || matchIndex == clientMatchIndex)                            
                GUI.Box(new Rect(10f + (scoreIndex * 110), 10f, 100f, 25f), $"{playerName}: {score}"); //$"{(playerNumber + 1).ToString()}: {score}");            

            // display name upon 3d character
            //if (isLocalPlayer)            
                GetComponentInChildren<Text>().text = playerName; //playersNames[playerNumber];                        
        }


        
    }
}
