using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    public float Speed;
        
    void Update()
    {
        transform.Rotate(Vector3.up , 2 * Speed);        
    }

}
