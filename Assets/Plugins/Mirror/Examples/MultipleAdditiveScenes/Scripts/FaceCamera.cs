using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{

    private GameObject _camera;

    void Start()
    {
        _camera = GameObject.Find("Main Camera");
    }

    
    void Update()
    {
        var fwd = _camera.transform.forward; //Camera.main.transform.forward;
        //fwd.y = 0.0f;
        transform.rotation = Quaternion.LookRotation(fwd);
    }

}
