// GENERATED AUTOMATICALLY FROM 'Assets/InputSystemMMO/PlayerInputsConfiguration.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputsConfiguration : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputsConfiguration()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputsConfiguration"",
    ""maps"": [
        {
            ""name"": ""Player Action Map"",
            ""id"": ""f8d6cb13-b3b5-4337-b60b-8b8af84aaad1"",
            ""actions"": [
                {
                    ""name"": ""CameraMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5826369e-0299-4fa3-b0a7-6d30bfe46f33"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PlayerMovement-Left"",
                    ""type"": ""Button"",
                    ""id"": ""cd6ef8c2-15a7-4eb7-a0c4-1605da12770c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PlayerMovement-Right"",
                    ""type"": ""Button"",
                    ""id"": ""47db9b30-6530-4a3b-9a4f-266990d94b8c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PlayerMovement-Up"",
                    ""type"": ""Button"",
                    ""id"": ""a7137e6f-0553-446d-8712-05522f40fe20"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PlayerMovement-Down"",
                    ""type"": ""Button"",
                    ""id"": ""f0252a4b-207a-4454-ab86-cbe960e0adae"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PlayerMovement-Forward"",
                    ""type"": ""Button"",
                    ""id"": ""d3acc22b-ec2b-47ee-bb46-c27a758cb1fa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9e16434d-7446-4126-a19f-e7828c5712b9"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Mouse"",
                    ""action"": ""CameraMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97788d39-535c-452e-b64a-590dedbad2d9"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PlayerMovement-Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2a1aca7f-1440-4830-9951-8db0b5daae2e"",
                    ""path"": ""<Joystick>/stick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""JoyStick"",
                    ""action"": ""PlayerMovement-Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c74f13f-930e-49cd-9341-4d6b9603cb7f"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PlayerMovement-Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f11332bf-9308-4a0a-979d-7382ff89abcc"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PlayerMovement-Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""63f545b7-9424-4c31-93e8-1966d67a0518"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PlayerMovement-Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d406894f-e6b0-447d-9b14-af6659dd4ace"",
                    ""path"": ""<Keyboard>/v"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""PlayerMovement-Forward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""P4_V2"",
            ""id"": ""1f7ec245-62b1-48ba-83b4-2c090ebfab91"",
            ""actions"": [
                {
                    ""name"": ""Axis Horizontal"",
                    ""type"": ""Button"",
                    ""id"": ""ab735120-3051-49c2-8227-5982462ff3a6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Axis Vertical"",
                    ""type"": ""Button"",
                    ""id"": ""71fe2b8b-ba6d-47bd-a3ba-5adcc04218b3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Forward"",
                    ""type"": ""Button"",
                    ""id"": ""35f3528f-294e-4c5b-8021-dc7920d7966a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis Horizontal"",
                    ""id"": ""6fda8ef3-08c2-4b45-a67e-88cf4a203d95"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Axis Horizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""bd02c9a8-dab5-4dfb-a64d-7981b32a541c"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Axis Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""66c0ac22-934b-41d1-afd4-ef930828ae5c"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Axis Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis Vertical"",
                    ""id"": ""6750c211-fb8c-416d-ac1c-ee652c529c52"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Axis Vertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ce623651-d83c-4ac9-99a1-83297ed588b2"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Axis Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""55cfb309-3824-44ea-93b2-522237417378"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Axis Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5b21a9c1-7cf0-4c54-a2de-6091fff821fd"",
                    ""path"": ""<Keyboard>/v"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Forward"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Mouse"",
            ""bindingGroup"": ""Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""JoyStick"",
            ""bindingGroup"": ""JoyStick"",
            ""devices"": [
                {
                    ""devicePath"": ""<Joystick>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player Action Map
        m_PlayerActionMap = asset.FindActionMap("Player Action Map", throwIfNotFound: true);
        m_PlayerActionMap_CameraMovement = m_PlayerActionMap.FindAction("CameraMovement", throwIfNotFound: true);
        m_PlayerActionMap_PlayerMovementLeft = m_PlayerActionMap.FindAction("PlayerMovement-Left", throwIfNotFound: true);
        m_PlayerActionMap_PlayerMovementRight = m_PlayerActionMap.FindAction("PlayerMovement-Right", throwIfNotFound: true);
        m_PlayerActionMap_PlayerMovementUp = m_PlayerActionMap.FindAction("PlayerMovement-Up", throwIfNotFound: true);
        m_PlayerActionMap_PlayerMovementDown = m_PlayerActionMap.FindAction("PlayerMovement-Down", throwIfNotFound: true);
        m_PlayerActionMap_PlayerMovementForward = m_PlayerActionMap.FindAction("PlayerMovement-Forward", throwIfNotFound: true);
        // P4_V2
        m_P4_V2 = asset.FindActionMap("P4_V2", throwIfNotFound: true);
        m_P4_V2_AxisHorizontal = m_P4_V2.FindAction("Axis Horizontal", throwIfNotFound: true);
        m_P4_V2_AxisVertical = m_P4_V2.FindAction("Axis Vertical", throwIfNotFound: true);
        m_P4_V2_Forward = m_P4_V2.FindAction("Forward", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player Action Map
    private readonly InputActionMap m_PlayerActionMap;
    private IPlayerActionMapActions m_PlayerActionMapActionsCallbackInterface;
    private readonly InputAction m_PlayerActionMap_CameraMovement;
    private readonly InputAction m_PlayerActionMap_PlayerMovementLeft;
    private readonly InputAction m_PlayerActionMap_PlayerMovementRight;
    private readonly InputAction m_PlayerActionMap_PlayerMovementUp;
    private readonly InputAction m_PlayerActionMap_PlayerMovementDown;
    private readonly InputAction m_PlayerActionMap_PlayerMovementForward;
    public struct PlayerActionMapActions
    {
        private @PlayerInputsConfiguration m_Wrapper;
        public PlayerActionMapActions(@PlayerInputsConfiguration wrapper) { m_Wrapper = wrapper; }
        public InputAction @CameraMovement => m_Wrapper.m_PlayerActionMap_CameraMovement;
        public InputAction @PlayerMovementLeft => m_Wrapper.m_PlayerActionMap_PlayerMovementLeft;
        public InputAction @PlayerMovementRight => m_Wrapper.m_PlayerActionMap_PlayerMovementRight;
        public InputAction @PlayerMovementUp => m_Wrapper.m_PlayerActionMap_PlayerMovementUp;
        public InputAction @PlayerMovementDown => m_Wrapper.m_PlayerActionMap_PlayerMovementDown;
        public InputAction @PlayerMovementForward => m_Wrapper.m_PlayerActionMap_PlayerMovementForward;
        public InputActionMap Get() { return m_Wrapper.m_PlayerActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActionMapActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActionMapActions instance)
        {
            if (m_Wrapper.m_PlayerActionMapActionsCallbackInterface != null)
            {
                @CameraMovement.started -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnCameraMovement;
                @CameraMovement.performed -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnCameraMovement;
                @CameraMovement.canceled -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnCameraMovement;
                @PlayerMovementLeft.started -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementLeft;
                @PlayerMovementLeft.performed -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementLeft;
                @PlayerMovementLeft.canceled -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementLeft;
                @PlayerMovementRight.started -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementRight;
                @PlayerMovementRight.performed -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementRight;
                @PlayerMovementRight.canceled -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementRight;
                @PlayerMovementUp.started -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementUp;
                @PlayerMovementUp.performed -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementUp;
                @PlayerMovementUp.canceled -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementUp;
                @PlayerMovementDown.started -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementDown;
                @PlayerMovementDown.performed -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementDown;
                @PlayerMovementDown.canceled -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementDown;
                @PlayerMovementForward.started -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementForward;
                @PlayerMovementForward.performed -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementForward;
                @PlayerMovementForward.canceled -= m_Wrapper.m_PlayerActionMapActionsCallbackInterface.OnPlayerMovementForward;
            }
            m_Wrapper.m_PlayerActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @CameraMovement.started += instance.OnCameraMovement;
                @CameraMovement.performed += instance.OnCameraMovement;
                @CameraMovement.canceled += instance.OnCameraMovement;
                @PlayerMovementLeft.started += instance.OnPlayerMovementLeft;
                @PlayerMovementLeft.performed += instance.OnPlayerMovementLeft;
                @PlayerMovementLeft.canceled += instance.OnPlayerMovementLeft;
                @PlayerMovementRight.started += instance.OnPlayerMovementRight;
                @PlayerMovementRight.performed += instance.OnPlayerMovementRight;
                @PlayerMovementRight.canceled += instance.OnPlayerMovementRight;
                @PlayerMovementUp.started += instance.OnPlayerMovementUp;
                @PlayerMovementUp.performed += instance.OnPlayerMovementUp;
                @PlayerMovementUp.canceled += instance.OnPlayerMovementUp;
                @PlayerMovementDown.started += instance.OnPlayerMovementDown;
                @PlayerMovementDown.performed += instance.OnPlayerMovementDown;
                @PlayerMovementDown.canceled += instance.OnPlayerMovementDown;
                @PlayerMovementForward.started += instance.OnPlayerMovementForward;
                @PlayerMovementForward.performed += instance.OnPlayerMovementForward;
                @PlayerMovementForward.canceled += instance.OnPlayerMovementForward;
            }
        }
    }
    public PlayerActionMapActions @PlayerActionMap => new PlayerActionMapActions(this);

    // P4_V2
    private readonly InputActionMap m_P4_V2;
    private IP4_V2Actions m_P4_V2ActionsCallbackInterface;
    private readonly InputAction m_P4_V2_AxisHorizontal;
    private readonly InputAction m_P4_V2_AxisVertical;
    private readonly InputAction m_P4_V2_Forward;
    public struct P4_V2Actions
    {
        private @PlayerInputsConfiguration m_Wrapper;
        public P4_V2Actions(@PlayerInputsConfiguration wrapper) { m_Wrapper = wrapper; }
        public InputAction @AxisHorizontal => m_Wrapper.m_P4_V2_AxisHorizontal;
        public InputAction @AxisVertical => m_Wrapper.m_P4_V2_AxisVertical;
        public InputAction @Forward => m_Wrapper.m_P4_V2_Forward;
        public InputActionMap Get() { return m_Wrapper.m_P4_V2; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(P4_V2Actions set) { return set.Get(); }
        public void SetCallbacks(IP4_V2Actions instance)
        {
            if (m_Wrapper.m_P4_V2ActionsCallbackInterface != null)
            {
                @AxisHorizontal.started -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnAxisHorizontal;
                @AxisHorizontal.performed -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnAxisHorizontal;
                @AxisHorizontal.canceled -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnAxisHorizontal;
                @AxisVertical.started -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnAxisVertical;
                @AxisVertical.performed -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnAxisVertical;
                @AxisVertical.canceled -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnAxisVertical;
                @Forward.started -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnForward;
                @Forward.performed -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnForward;
                @Forward.canceled -= m_Wrapper.m_P4_V2ActionsCallbackInterface.OnForward;
            }
            m_Wrapper.m_P4_V2ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @AxisHorizontal.started += instance.OnAxisHorizontal;
                @AxisHorizontal.performed += instance.OnAxisHorizontal;
                @AxisHorizontal.canceled += instance.OnAxisHorizontal;
                @AxisVertical.started += instance.OnAxisVertical;
                @AxisVertical.performed += instance.OnAxisVertical;
                @AxisVertical.canceled += instance.OnAxisVertical;
                @Forward.started += instance.OnForward;
                @Forward.performed += instance.OnForward;
                @Forward.canceled += instance.OnForward;
            }
        }
    }
    public P4_V2Actions @P4_V2 => new P4_V2Actions(this);
    private int m_MouseSchemeIndex = -1;
    public InputControlScheme MouseScheme
    {
        get
        {
            if (m_MouseSchemeIndex == -1) m_MouseSchemeIndex = asset.FindControlSchemeIndex("Mouse");
            return asset.controlSchemes[m_MouseSchemeIndex];
        }
    }
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    private int m_JoyStickSchemeIndex = -1;
    public InputControlScheme JoyStickScheme
    {
        get
        {
            if (m_JoyStickSchemeIndex == -1) m_JoyStickSchemeIndex = asset.FindControlSchemeIndex("JoyStick");
            return asset.controlSchemes[m_JoyStickSchemeIndex];
        }
    }
    public interface IPlayerActionMapActions
    {
        void OnCameraMovement(InputAction.CallbackContext context);
        void OnPlayerMovementLeft(InputAction.CallbackContext context);
        void OnPlayerMovementRight(InputAction.CallbackContext context);
        void OnPlayerMovementUp(InputAction.CallbackContext context);
        void OnPlayerMovementDown(InputAction.CallbackContext context);
        void OnPlayerMovementForward(InputAction.CallbackContext context);
    }
    public interface IP4_V2Actions
    {
        void OnAxisHorizontal(InputAction.CallbackContext context);
        void OnAxisVertical(InputAction.CallbackContext context);
        void OnForward(InputAction.CallbackContext context);
    }
}
