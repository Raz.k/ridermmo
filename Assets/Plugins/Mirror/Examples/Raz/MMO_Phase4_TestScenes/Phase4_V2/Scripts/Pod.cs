using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class Pod : NetworkBehaviour
    {
        public GameObject GameManager;
        public GameObject StarShip;
        public int ServerStage = 0;
        public Vector3 StartPosition;
        public Vector3 EndPosition;// = new Vector3(-128.3f, 48.5f, 1128f);
        public float Journey = 0;
        public float Journey2 = 0;
        public Transform PosOnMatherShip;
        public GameObject PodCollider;
        bool BackOnMatherShip = false;
        public float Speed;
        public float BackSpeed;

        [ServerCallback]
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "TubesPose")
            {
                ServerStage = 3;
                print("Stage = 3");
                //GetComponent<BoxCollider>().enabled = false;                
            }
            if (other.tag == "SpaceFighter" && GameManager.GetComponent<ScenarioPlayer>().ServerStage == 10)
            {                
                print("Stage = 11");                
                GameObject[] sfs2 = GameObject.FindGameObjectsWithTag("SpaceFighter");
                foreach (var sf2 in sfs2) 
                { 
                    sf2.GetComponent<P4_V2_SpaceFighter>().MovingToPod = false;
                    sf2.GetComponent<P4_V2_SpaceFighter>().BackOnPod();
                    sf2.transform.rotation = transform.rotation;
                }
                GameManager.GetComponent<ScenarioPlayer>().ServerStage = 11;
                PodCollider.SetActive(true);
            }
            //if (other.tag == "MotherShip")
            //{
            //    BackOnMatherShip = true;
            //    GameManager.GetComponent<ScenarioPlayer>().ServerStage = 12;
            //}
            if (other.tag == "BackToMS" && GameManager.GetComponent<ScenarioPlayer>().ServerStage != 12)
            {
                BackOnMatherShip = true;
                GameManager.GetComponent<ScenarioPlayer>().ServerStage = 12;
            }
            
        }

        private void Update()
        {
            if (GameManager.GetComponent<ScenarioPlayer>().ServerStage == 1 )
            {
                transform.position = StarShip.transform.position + new Vector3(0, 56.4f, 0);                
                ServerStage = 1;
                StartPosition = transform.position;
            }

            if (GameManager.GetComponent<ScenarioPlayer>().ServerStage == 6)
            {
                transform.position = Vector3.Lerp(StartPosition, EndPosition, Journey);
                Journey += 0.005f;
            }
            if (ServerStage == 11 && !BackOnMatherShip)
            {
                transform.position -=  new Vector3(0,0, BackSpeed);



                //Journey2 = 0.005f * Time.deltaTime;
                //transform.position = Vector3.Lerp(EndPosition, StartPosition, Journey2);

                //float step = Speed * Time.deltaTime;
                //transform.position = Vector3.MoveTowards(EndPosition, StartPosition, step);
                //print("pod 11");
            }
            if (GameManager.GetComponent<ScenarioPlayer>().ServerStage == 12)
            {
                transform.position = PosOnMatherShip.position;
            }
        }




    }
}