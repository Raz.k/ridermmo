using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class GameTimer : NetworkBehaviour
    {
        //Stages 
        // 1 -  star-field ride
        // 2 -  pod from mother-ship to tubes 
        // 3 - 20s ride inside tube 
        //   - 4s next stage explanation 
        // 4 - 20s inside the Dome shotting enemies
        // 5 - 5s automatic movement back to pod
        // 6 - 20s pod moving back to mother-ship while gunning enemies        

        public ScenarioPlayer ScenarioPlayer;
        public P4_V2_RingsObserver Comp;

        void Awake()
        {
            ScenarioPlayer = GetComponent<ScenarioPlayer>();            
        }

        public float timeRemaining;

        void Update()
        {
            if (timeRemaining >= 0)
            {
                timeRemaining -= Time.deltaTime;
                Comp.SetClock(Mathf.FloorToInt(timeRemaining % 60).ToString());
            }
        }


                         

    }
}