﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace Mirror.Examples.MMO_SpaceShip
{

    [RequireComponent(typeof(AudioSource))]
    public class SimpleCollectibleScript : NetworkBehaviour
    {

        public enum CollectibleTypes { NoType, Type1, Type2, Type3, Type4, Type5 }; // you can replace this with your own labels for the types of collectibles in your game!
        public CollectibleTypes CollectibleType;
        public bool rotate;
        public float rotationSpeed;
        public AudioClip collectSound;
        public GameObject collectEffect;
        public P4_V2_RingsObserver P4_V2_RingsObserver;

        public enum Colors { Red, Blue, Green };
        public Colors colors;
        public Material Red;
        public Material Blue;
        public Material Green;


        void Start()
        {
            if (colors == Colors.Red)
                GetComponent<Renderer>().material = Red;
            if (colors == Colors.Blue)
                GetComponent<Renderer>().material = Blue;
            if (colors == Colors.Green)
                GetComponent<Renderer>().material = Green;

            P4_V2_RingsObserver = GameObject.Find("InGameCanvas").GetComponent<P4_V2_RingsObserver>();
        }

        void Update()
        {
            if (rotate)
                transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime, Space.World);
        }

        [ServerCallback]
        void OnTriggerEnter(Collider other)
        { if (other.tag == "SpaceFighter")          
           if (other.GetComponent<P4_V2_SpaceFighter>().colors.ToString() == transform.tag)                
               Collect();                                
        }

        public void Collect()
        {
            if (collectSound)
                AudioSource.PlayClipAtPoint(collectSound, transform.position);
            if (collectEffect)
            {
                GameObject effect = Instantiate(collectEffect, transform.position, Quaternion.identity);
                NetworkServer.Spawn(effect);
            }

            if (transform.tag == "Red")
            {
                P4_V2_RingsObserver.RedScore++;
            }
            if (transform.tag == "Blue")
            {
                P4_V2_RingsObserver.BlueScore++;
            }
            if (transform.tag == "Green")
            {
                P4_V2_RingsObserver.GreenScore++;
            }

            ////Below is space to add in your code for what happens based on the collectible type
            //if (CollectibleType == CollectibleTypes.NoType) {

            //	//Add in code here;

            //	Debug.Log ("Do NoType Command");
            //}
            //if (CollectibleType == CollectibleTypes.Type1) {

            //	//Add in code here;

            //	Debug.Log ("Do NoType Command");
            //}
            //if (CollectibleType == CollectibleTypes.Type2) {

            //	//Add in code here;

            //	Debug.Log ("Do NoType Command");
            //}
            //if (CollectibleType == CollectibleTypes.Type3) {

            //	//Add in code here;

            //	Debug.Log ("Do NoType Command");
            //}
            //if (CollectibleType == CollectibleTypes.Type4) {

            //	//Add in code here;

            //	Debug.Log ("Do NoType Command");
            //}
            //if (CollectibleType == CollectibleTypes.Type5) {

            //Add in code here;

            //Debug.Log ("Do NoType Command");
            //}

            Destroy(gameObject);
        }
    }

}