using PathCreation.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class ScenarioPlayer : NetworkBehaviour
    {
        bool PausePart1 = false;
        bool Stage7Bool = false;
        bool Stage11Bool = false;
        bool Stage1Finished = false;
        P4_V2_RingsObserver comp;

        public GameObject GameCanvas;
        public GameObject StarField;
        public GameObject StarShip;
        public GameObject StarShipContainer;
        public GameObject Pod;
        public string ClientsTagName = "";

        [Header("Enemies")]
        public GameObject Enemy1;
        public GameObject Enemy2;
        public Transform SpawnPoint1;
        public Transform SpawnPoint2;        

        //Stages 
        // 1 - star-field ride
        // 2 - pod from mother-ship to tubes 
        // 3 - ride inside tube 
        // 4 - inside the Dome shotting enemies
        // 5 - back to pod
        // 6 - pod moving back to mother-ship while gunning enemies

        public int PlayerStage;
        [SyncVar(hook = nameof(UpdateClientsAbout_Stage))] public int ServerStage;
        private void UpdateClientsAbout_Stage(int _old, int _new) { PlayerStage = _new; }

        public int NumberOfPlayersForStartPlaying;
        public int PlayersLocalCount;
        [SyncVar(hook = nameof(UpdateClientsAboutPlayers))] public int Players;
        private void UpdateClientsAboutPlayers(int _old, int _new) { PlayersLocalCount = _new; }

        [SyncVar(hook = nameof(UpdateClientsAboutDockStatus))] public bool IsFighters_Dock = true;        
        private void UpdateClientsAboutDockStatus(bool _old, bool _new) { StartCoroutine(DelayIt()); }

        private void Awake()
        {
            comp = GameCanvas.GetComponent<P4_V2_RingsObserver>();
        }


        

        void Update()
        {
            if (isServer)//only server is allowed to announce player count since he is the only that can count them
            {
                Players = NetworkServer.connections.Count;
                if (NetworkServer.connections.Count == NumberOfPlayersForStartPlaying)
                {
                    NumberOfPlayersForStartPlaying = 0; // avoid running again                    
                    ServerStage = 1;
                }
                if (ServerStage == 1 && !Stage1Finished)
                {
                    StarShipContainer.GetComponent<PathShipFollower>().Follow = true;                    
                    StarField.SetActive(true);
                    comp.SetTypingText("Star-field ride");
                    Stage1Finished = true;
                }
                if (StarShip.GetComponent<MotherShip>().ServerStage == 2)
                {
                    StarShipContainer.GetComponent<PathShipFollower>().Follow = false;
                    ServerStage = 2;
                    StarField.SetActive(false);
                    comp.SetText("Mother-ship landed");
                    StartCoroutine(Part1());
                    if (PausePart1)
                    {
                        Pod.transform.position += new Vector3(0, 0, 2);                        
                        comp.SetTypingText("Pod moving from mother-ship to tubes");
                    }
                }
                if (Pod.GetComponent<Pod>().ServerStage == 3)
                {
                    GameCanvas.GetComponent<P4_V2_RingsObserver>().SetText("");
                    StarShip.GetComponent<MotherShip>().ServerStage = 3;
                    ServerStage = 3;
                    IsFighters_Dock = false;                    
                }
                if (ServerStage == 6 )
                {
                
                }
                if (ServerStage == 7 && !Stage7Bool)
                {
                    Stage7Bool = true;// dont enter again
                    StartCoroutine(InDome());
                }
                if (ServerStage == 11)
                {
                    if (!Stage11Bool)
                    {
                        StartCoroutine(BackToMotherShip());
                        Stage11Bool = true;
                        print("scenario ServerStage == 11");
                    }
   

                    SpawnPoint1.position = Pod.transform.position + new Vector3(0, 0, 350);
                    SpawnPoint2.position = Pod.transform.position + new Vector3(0, 0, 400);
                }
                if (ServerStage == 12)
                {
                    comp.SetTypingText("Back on Mother Ship - Mission Accomplished ");
                }

            }           
        }

        IEnumerator Part1()
        {
            yield return new WaitForSeconds(3);
            PausePart1 = true;
        }

        // every client gets all his instance SpaceFighter's
        IEnumerator DelayIt()
        {
            yield return new WaitForSeconds(3);
            GetComponent<GameTimer>().timeRemaining = 5;
            Pod.GetComponent<Pod>().ServerStage = 4;
            ServerStage = 5;
            comp.SetText("5 - taxing into tube ");
            GameObject[] sfs = GameObject.FindGameObjectsWithTag(ClientsTagName);
            foreach (var sf in sfs) { sf.GetComponent<P4_V2_SpaceFighter>().MovingToTube = true; }

            yield return new WaitForSeconds(2);
            comp.SetTypingText("your mission - fly and collect the rings along the tube - Get Ready!");
            yield return new WaitForSeconds(10);

            //yield return new WaitForSeconds(5);
            comp.SetTypingText("6 - You got the controls! Go!");
            // find fighter and updates about docking status 
            GameObject[] sfs1 = GameObject.FindGameObjectsWithTag(ClientsTagName);
            foreach (var sf1 in sfs1) { sf1.GetComponent<P4_V2_SpaceFighter>().IsFollow = false; }
            ServerStage = 6;
            GetComponent<GameTimer>().timeRemaining = 70;

            yield return new WaitForSeconds(3);
            comp.SetText("");
        }

        IEnumerator InDome()
        {
            comp.SetTypingText("You are in the Dome , Enemies inside the dome will appear soon");
            yield return new WaitForSeconds(5);
            
            comp.SetText("");
            comp.SetTypingText("Your mission - defend your self! squeeze the trigger! ");                              
            yield return new WaitForSeconds(6);            
            
            GameObject[] sfs = GameObject.FindGameObjectsWithTag(ClientsTagName);
            foreach (var sf in sfs) { sf.GetComponent<P4_V2_SpaceFighter>().SetTimer = true; }

            GetComponent<GameTimer>().timeRemaining = 3;
            yield return new WaitForSeconds(3);
            LunchEnemyWave(15);
            comp.SetText("");

            GetComponent<GameTimer>().timeRemaining = 20;
            yield return new WaitForSeconds(20);

            LunchEnemyWave(15);

            comp.SetTypingText("You are pulled back to your teams Pod");
            GameObject[] sfs2 = GameObject.FindGameObjectsWithTag(ClientsTagName);
            foreach (var sf2 in sfs2) { sf2.GetComponent<P4_V2_SpaceFighter>().MovingToPod = true; }

            ServerStage = 10;
        }


        IEnumerator BackToMotherShip()
        {
            comp.SetTypingText("Pod is moving back to mother-ship ");
            yield return new WaitForSeconds(3);
            comp.SetText("");
            comp.SetTypingText("Your mission - Defend your pod team  ");
            yield return new WaitForSeconds(3);
            Pod.GetComponent<Pod>().ServerStage = 11;
            LunchEnemyWave(10);
            yield return new WaitForSeconds(15);
            SpawnPoint1.position = Pod.transform.position + new Vector3(0, 100, 350);
            SpawnPoint2.position = Pod.transform.position + new Vector3(0, 80, 400);
            comp.SetText("");
            LunchEnemyWave(10);

            yield return new WaitForSeconds(10);
            SpawnPoint1.position = Pod.transform.position + new Vector3(-100, 200, 350);
            SpawnPoint2.position = Pod.transform.position + new Vector3(-50, 150, 400);
            LunchEnemyWave(10);

            yield return new WaitForSeconds(12);
            SpawnPoint1.position = Pod.transform.position + new Vector3(100, 200, 350);
            SpawnPoint2.position = Pod.transform.position + new Vector3(50, 150, 400);
            LunchEnemyWave(10);

            yield return new WaitForSeconds(10);
            SpawnPoint1.position = Pod.transform.position + new Vector3(-100, 100, 350);
            SpawnPoint2.position = Pod.transform.position + new Vector3(100, 100, 400);
            LunchEnemyWave(10);
        }
        
        
        
        public void LunchEnemyWave(int number)
        {
            for (int i = 0; i < number; i++)
            {
                GameObject projectileRight = Instantiate(Enemy1, SpawnPoint1.position, transform.rotation);
                NetworkServer.Spawn(projectileRight);
                GameObject projectileLeft = Instantiate(Enemy2, SpawnPoint2.position, transform.rotation);
                NetworkServer.Spawn(projectileLeft);
            }
        }




    }
}
