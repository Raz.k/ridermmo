using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class P4_V2_Inputs : NetworkBehaviour
    {
        [Header("Movement")]
        public float rotationSpeed = 100;
        public float horizontal;
        public float vertical;
        Vector2 MovementValues;

        public bool Left;
        public bool Up;
        public bool Right;
        public bool Down;

        public float Thrust = 10;
        public float MaxThrust = 40;
        public float CameraInput;

        P4_V2_SpaceFighter SpaceFighter;
        public PlayerInputsConfiguration PlayerInputs;
        

        #region for C# events 
        private void Awake()
        {
            PlayerInputs = new PlayerInputsConfiguration();                        
        }
        private void OnEnable()
        {
            PlayerInputs. P4_V2.Forward.performed += Forward_performed;
        }
        private void OnDisable()
        {
            PlayerInputs.P4_V2.Forward.performed -= Forward_performed;
        }
        private void Forward_performed(InputAction.CallbackContext context)
        {
            print( context.ReadValue<float>());
        }
        #endregion





        private void Start() { SpaceFighter = GetComponent<P4_V2_SpaceFighter>();  }

        public void CameraMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer)// will happen once 
            {
                if (context.performed)
                {
                    MovementValues = context.ReadValue<Vector2>();
                    horizontal = MovementValues.x;
                    if (MovementValues.x > 1) horizontal /= 20;
                    if (MovementValues.x < -1) horizontal /= 20;

                    vertical = MovementValues.y;
                    if (MovementValues.y > 1) vertical /= 20;
                    if (MovementValues.y < -1) vertical /= 20;
                }
            }
        }


        public void PlayerLeftMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Left = true;
            if (isLocalPlayer && context.canceled) Left = false;
        }
        public void PlayerRightMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Right = true;
            if (isLocalPlayer && context.canceled) Right = false;
        }
        public void PlayerUpMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Up = true;
            if (isLocalPlayer && context.canceled) Up = false;
        }
        public void PlayerDownMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Down = true;
            if (isLocalPlayer && context.canceled) Down = false;
        }
        public void PlayerForwardMovement(InputAction.CallbackContext context)
        {
            print(context.ReadValue<float>());
            if (isLocalPlayer && context.performed && Thrust < 60) Thrust += 10f;
            //if (isLocalPlayer && context.canceled) Thrust -= 1f;
        }


        public void PlayerEnginePower(InputAction.CallbackContext context)
        {
            //print(context.ReadValue<float>());

            //keyboard
            if (isLocalPlayer && context.performed && context.ReadValue<float>() == 1 && Thrust > 10)
                Thrust -= 10f;
            if (isLocalPlayer && context.performed && context.ReadValue<float>() == -1 && Thrust < 50)
                Thrust += 10f;
            
            //joystick
            if (isLocalPlayer && context.performed)
            {                 
                // 1 = no power 
                // -1 = max power
                Thrust = (context.ReadValue<float>() - 1) * -50;                
            }
        }   

        public void PlayerBackwardMovement(InputAction.CallbackContext context)
        {
            print(context.ReadValue<float>());
            if (isLocalPlayer && context.performed && Thrust > 10) Thrust -= 10f;            
        }
        public void PlayerMovementHorizontal(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed)                            
                horizontal = context.ReadValue<float>() /5f;            
            if (isLocalPlayer && context.canceled)
                horizontal = 0;
        }
        public void PlayerMovementVertical(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed)                            
                vertical = context.ReadValue<float>() / 5f;
            if (isLocalPlayer && context.canceled)
                vertical = 0;
        }

        public void CameraSwitch(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed)            
                CameraInput = context.ReadValue<float>();
            if (isLocalPlayer && context.canceled)
                CameraInput = 0;
            
        }

        public void Fire(InputAction.CallbackContext context) 
        {
            SpaceFighter.CmdFire();
        }

    }
}