using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mirror.Examples.MMO_SpaceShip
{
    public class Enemy : NetworkBehaviour
    {
        
        public GameObject ExplosionPrefab;
        public Vector3 PatrolTarget;
        public float SearchRadius;
        public float AttackRadios;
        public string Tag;
        public float MoveToPlayerSpeed;
        Vector3 _playerPosition;
        float _playerDistance;


        [ServerCallback]
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Shot")
            {                
                GameObject explosion = Instantiate(ExplosionPrefab, transform.position, transform.rotation);
                NetworkServer.Spawn(explosion);
                NetworkServer.Destroy(gameObject);
            }
        }

        private void Update()
        {
            // search for food and player
            Collider[] hitObjects = Physics.OverlapSphere(transform.position, SearchRadius);
            for (int i = 0; i < hitObjects.Length; i++)
            {
                if (hitObjects[i].CompareTag(Tag))
                    _playerPosition = hitObjects[i].gameObject.transform.position;                
            }
            Brain(_playerPosition);
        }

        private void Brain(Vector3 _playerPosition)
        {
            _playerDistance = (transform.position - _playerPosition).magnitude;

            if (_playerPosition == Vector3.zero)// -> no player found
            {                
                PatrolOrStand();               
            }
            else // -> player found 
            {
                if (_playerDistance < AttackRadios && _playerPosition != Vector3.zero)
                    MoveToPlayer(_playerPosition);
            }
        }

        private void MoveToPlayer(Vector3 playerPosition)
        {            
            transform.position = Vector3.MoveTowards(transform.position, playerPosition, MoveToPlayerSpeed * Time.deltaTime);
        }

        private void PatrolOrStand()
        {
            transform.position = Vector3.MoveTowards(transform.position, PatrolTarget, MoveToPlayerSpeed * Time.deltaTime);
        }
    }

}