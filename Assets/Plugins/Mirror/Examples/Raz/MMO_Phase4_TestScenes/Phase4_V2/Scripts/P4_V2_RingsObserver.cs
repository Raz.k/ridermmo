using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class P4_V2_RingsObserver : NetworkBehaviour
    {
        [SerializeField] public Text Message;
        [SerializeField] public Text Clock;
        [SerializeField] public AudioSource AudioSource;
        [SerializeField] public AudioClip Audio;


        [SerializeField] public Text RedScoreText;
        [SerializeField] public Text BlueScoreText;
        [SerializeField] public Text GreenScoreText;                

        [SyncVar(hook = nameof(UpdateClientsAboutRed))]
        public int RedScore;
        [SyncVar(hook = nameof(UpdateClientsAboutBlue))]
        public int BlueScore;
        [SyncVar(hook = nameof(UpdateClientsAboutGreen))]
        public int GreenScore;

        private void Start() { RedScore = BlueScore = 0; }

        public void SetText(string text )
        { Message.text = text; }
        public void SetTypingText(string text)
        {
            GetComponent<TextTyper>().SetText(text);            
        }

        public void SetClock(string text)
        {if (text != "-1") Clock.text = text; }


        private void UpdateClientsAboutRed(int oldScore, int newScore)
        {
            RedScore = newScore;
            RedScoreText.text = RedScore.ToString();
            //PlaySound(Audio);
        }
        private void UpdateClientsAboutBlue(int oldScore, int newScore)
        {
            BlueScore = newScore;
            BlueScoreText.text = BlueScore.ToString();
            //PlaySound(Audio);
        }
        private void UpdateClientsAboutGreen(int oldScore, int newScore)
        {
            GreenScore = newScore;
            GreenScoreText.text = GreenScore.ToString();
            //PlaySound(Audio);
        }



        public void PlaySound(AudioClip clip)
        {
            AudioSource.clip = clip;
            AudioSource.Play();
        }


    }
}