using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation.Examples;


namespace Mirror.Examples.MMO_SpaceShip
{
    public class MotherShip : MonoBehaviour
    {
        public int ServerStage;
        public GameObject InGameCanvas;

        [ServerCallback]
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "SlowSpeed")
            {
                print("SlowSpeed");
                GetComponentInParent<PathShipFollower>().speed = 5;
                InGameCanvas.GetComponent<P4_V2_RingsObserver>().SetTypingText("2 - Slowing down towards destination");
            }
            if (other.tag == "ZeroPose")
            {
                ServerStage = 2;
                print("Stage = 2");
                GetComponent<BoxCollider>().enabled = false;
            }            
        }

        
    }
}