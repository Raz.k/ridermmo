using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Mirror.Examples.MMO_SpaceShip
{
    public class TextTyper : MonoBehaviour
    {
        public Text Txt;
        public string story;
        public float Ratio = 0.125f;

        public void SetText(string TextToType)
        {
            Txt =  GameObject.Find("Message").GetComponent<Text>();
            story = TextToType; //Txt.text;
            Txt.text = "";

            // TODO: add optional delay when to start
            StartCoroutine(PlayText());
        }


        IEnumerator PlayText()
        {
            foreach (char c in story)
            {
                Txt.text += c;
                yield return new WaitForSeconds(Ratio);
            }
        }
    }

}