using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
//using Cinemachine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class P4_V2_SpaceFighter : NetworkBehaviour
    {

        public float MovementVal = 0.02f;
        public GameObject ThierdPCamera;
        public GameObject CinemachineThierdController;
        public GameObject FirstPCamera;
        public GameObject CinemachineFirstController;
        public GameObject ThierdPCameraRegular;
        P4_V2_RingsObserver P4_V2_RingsObserver;
        P4_V2_Inputs P4_V2_Inputs;
        Phase4PathColor phase4PathColor;
        Transform PlayingStation;
        AudioSource audioSource;
        GameObject GameManager;
        Phase4Manager Phase4Manager;


        // for cam rotation clamp
        float rotX;
        float rotY;
        float speed = 30;

        float pitch;
        float yaw;
        float roll;
        Vector3 vec;        
        float maxAngle = 20f;

        float curAngle = 0f;


        bool camBool = true;
        bool done = false;
        public bool MovingToTube = false;
        public bool SetTimer = false;
        public bool MovingToPod = false;
        public bool LimitControls = false;

        // colors
        Material mat;
        public GameObject Cockpit3_Body;
        public GameObject Cockpit3_Interior;
        public GameObject MainBody2;
        public GameObject TrustreGlowLeft;
        public GameObject TrustreGlowRight;

        public float TubeTextureOffsetY = 0;
        public enum Colors { Red, Blue, Green }
        public Colors colors;

        [Header("Projectile")]
        public GameObject projectilePrefab;
        public Transform projectileMountRight;
        public Transform projectileMountLeft;
        public Transform projectileMountCenter;


        // SyncVar's        
        [SyncVar(hook = nameof(UpdateFollowStat))] public bool IsFollow;
        private void UpdateFollowStat(bool _old, bool _new)
        {
            if (_new)
            {
                print("IsFollow in fighter");
                //ThierdPCamera.SetActive(true);
                //FirstPCamera.SetActive(false);
                //Cinemachine.SetActive(true);
            }
        }

        [SyncVar(hook = nameof(UpdateDomeStat))] public bool IsInDome;
        private void UpdateDomeStat(bool _old, bool _new)
        {
            if (_new)
            {
                print(" fighter in IsInDome ");
            }
        }



        [SyncVar(hook = nameof(UpdatePlayerNumber))] public int PlayerNumber = 0;
        private void UpdatePlayerNumber(int _old, int _new) { PlayerNumber = _new; }

        private void Awake()
        {
            GameManager = GameObject.Find("GameManager");
            P4_V2_RingsObserver = GameObject.Find("InGameCanvas").GetComponent<P4_V2_RingsObserver>();
            P4_V2_Inputs = GetComponent<P4_V2_Inputs>();
            audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            if (isLocalPlayer)
            {
                PlayerNumber = GameManager.GetComponent<ScenarioPlayer>().Players;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                P4_V2_Inputs.enabled = true;
                GetComponent<PlayerInput>().enabled = true;
            }

            // position the player on the playing station
            if (PlayerNumber == 0)
                PlayerNumber = GameManager.GetComponent<ScenarioPlayer>().Players;
            PlayingStation = GameObject.Find("Pos" + PlayerNumber).transform;//GameObject.Find("WarpSpeed_Tunnel" + PlayerNumber).transform;
            transform.parent = PlayingStation;
            transform.position = PlayingStation.position;

            // to avoid scale value 
            //transform.localScale = new Vector3(0.0025f, 0.0025f, 0.0025f);
            audioSource.enabled = true;
            if (isLocalPlayer)
            {
                if (isClient)
                {
                    CinemachineFirstController.SetActive(true);
                    //ThierdPCamera.GetComponent<Camera>().enabled = false;
                    FirstPCamera.SetActive(true);
                    // FirstPCamera.GetComponent<Camera>().enabled = true;
                    FirstPCamera.GetComponent<AudioListener>().enabled = true;
                }
            }


            // color
            phase4PathColor = PlayingStation.GetComponent<Phase4PathColor>();

            if (phase4PathColor.Color.ToString() == "Red")
            {
                colors = Colors.Red;
                mat = phase4PathColor.Red;
                TrustreGlowLeft.GetComponent<TrailRenderer>().material = mat;
                TrustreGlowRight.GetComponent<TrailRenderer>().material = mat;
                Cockpit3_Body.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
                Cockpit3_Interior.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
                MainBody2.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
            }
            if (phase4PathColor.Color.ToString() == "Blue")
            {
                colors = Colors.Blue;
                mat = phase4PathColor.Blue;
                TrustreGlowLeft.GetComponent<TrailRenderer>().material = mat;
                TrustreGlowRight.GetComponent<TrailRenderer>().material = mat;
                Cockpit3_Body.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
                Cockpit3_Interior.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
                MainBody2.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
            }
            if (phase4PathColor.Color.ToString() == "Green")
            {
                colors = Colors.Green;
                mat = phase4PathColor.Green;
                TrustreGlowLeft.GetComponent<TrailRenderer>().material = mat;
                TrustreGlowRight.GetComponent<TrailRenderer>().material = mat;
                Cockpit3_Body.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
                Cockpit3_Interior.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
                MainBody2.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
            }

        }

        private void Update()
        {

            if (MovingToPod)
            {
                float step = speed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, transform.parent.transform.position, step);
                transform.LookAt(transform.parent.transform);
            }

            // Moving  To Tube
            if (MovingToTube)
            {
                transform.parent = GameObject.Find("WarpSpeed_Tunnel" + PlayerNumber).transform;
                transform.parent.GetComponent<MeshRenderer>().enabled = true;
                float step = speed * Time.deltaTime; // calculate distance to move
                transform.position = Vector3.MoveTowards(transform.position, transform.parent.GetComponent<Tube>().StartPosition.transform.position, step);
            }

            if (!IsFollow)// no controls 
            {
                if (isLocalPlayer && !done)
                {
                    GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

                    FirstPCamera.SetActive(false);
                    CinemachineFirstController.SetActive(false);

                    ThierdPCamera.SetActive(true);
                    ThierdPCamera.GetComponent<AudioListener>().enabled = true;
                    CinemachineThierdController.SetActive(true);
                    MovingToTube = false;
                    done = true;
                }


                // moving player to dome when tube time is finished 
                if (!IsInDome && GameManager.GetComponent<GameTimer>().timeRemaining <= 0)
                {
                    transform.position = transform.parent.GetComponent<Tube>().EndPosition.transform.position;
                    IsInDome = true;
                }



                if (!LimitControls)
                {
                    //#############################################################################################
                    // fighter direction -  rotate            
                    pitch = P4_V2_Inputs.vertical * 2f * P4_V2_Inputs.rotationSpeed * Time.deltaTime;
                    yaw = P4_V2_Inputs.horizontal * P4_V2_Inputs.rotationSpeed * Time.deltaTime;
                    roll = -P4_V2_Inputs.horizontal * 200 * Time.deltaTime;

                    vec = new Vector3(pitch, yaw, roll);                    
                    transform.Rotate(vec);
                    //#############################################################################################


                    //// lean left
                    //if (P4_V2_Inputs.horizontal > 0)
                    //{
                    //    curAngle = Mathf.MoveTowardsAngle(curAngle, maxAngle, speed * Time.deltaTime);
                    //}
                    //// lean right
                    //else if (P4_V2_Inputs.horizontal < 0)
                    //{
                    //    curAngle = Mathf.MoveTowardsAngle(curAngle, -maxAngle, speed * Time.deltaTime);
                    //}
                    //// reset lean
                    //else
                    //{
                    //    curAngle = Mathf.MoveTowardsAngle(curAngle, 0f, speed * Time.deltaTime);
                    //}

                    //transform.localRotation = Quaternion.AngleAxis(curAngle, -Vector3.forward);


                    //transform.Rotate(P4_V2_Inputs.vertical * 2f * P4_V2_Inputs.rotationSpeed * Time.deltaTime,
                    //         //0,
                    //         ///-P4_V2_Inputs.horizontal * 3 * P4_V2_Inputs.rotationSpeed * Time.deltaTime);
                    //         P4_V2_Inputs.horizontal * P4_V2_Inputs.rotationSpeed * Time.deltaTime,
                    //         -P4_V2_Inputs.horizontal * 200 * Time.deltaTime);






                    // fighter movement power                        
                    transform.position += transform.forward * P4_V2_Inputs.Thrust * Time.deltaTime;
                    if (P4_V2_Inputs.Thrust > 10)
                    {
                        audioSource.pitch = P4_V2_Inputs.Thrust / 10;//1.5f;

                        //// camera shack
                        //float x = Random.Range(-0.01f, 0.01f) * 0.08f;
                        //float y = Random.Range(-0.01f, 0.01f) * 0.08f;
                        //FPCamera.transform.localPosition = new Vector3(x, y, 0);
                    }
                    else
                    {
                        audioSource.pitch = 1f;
                        ThierdPCamera.transform.localPosition = new Vector3(0, 0.05f, 0.09f);
                    }
                }
                else
                {
                    // Gunner rotation
                    rotX = P4_V2_Inputs.vertical * P4_V2_Inputs.rotationSpeed * Time.deltaTime;
                    rotX = Mathf.Clamp(rotX, -15, 15f);
                    rotY = P4_V2_Inputs.horizontal * P4_V2_Inputs.rotationSpeed * Time.deltaTime;
                    rotY = Mathf.Clamp(rotY, -15f, 15f);

                    transform.Rotate(rotX, rotY, 0);
                    //transform.localPosition = Vector3.zero;
                    //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
                }

                //// camera rotation 
                //if (ThierdPCameraRegular.activeInHierarchy)
                //{
                //    rotX += P4_V2_Inputs.vertical * P4_V2_Inputs.rotationSpeed * Time.deltaTime;
                //    rotX = Mathf.Clamp(rotX, -15f, 15f);
                //    rotY += P4_V2_Inputs.horizontal * P4_V2_Inputs.rotationSpeed * Time.deltaTime;
                //    rotY = Mathf.Clamp(rotY, -15f, 15f);
                //    ThierdPCameraRegular.transform.localRotation = Quaternion.Euler(rotX, rotY, 0f);
                //}

                // camera first to third person 
                if (P4_V2_Inputs.CameraInput == 1)
                {
                    ThierdPCamera.transform.localPosition = new Vector3(0, 2.3f, -12f);
                    //FPCamera.transform.localPosition += new Vector3(0, 0, 0);
                }

                // tube texture animation
                TubeTextureOffsetY += 0.00003f;
                transform.parent.GetComponent<Renderer>().material.SetTextureOffset("_BaseColorMap", new Vector2(TubeTextureOffsetY, TubeTextureOffsetY));
            }
        }

        private void LateUpdate()
        {
            // limit movement to bounds              
            //Vector3 currentPose = transform.localPosition;
            //currentPose.x = Mathf.Clamp(currentPose.x, -5f, 5f);
            //currentPose.y = Mathf.Clamp(currentPose.y, -5f, 5f);
            //transform.localPosition = currentPose;
        }


        [Command]
        public void CmdFire()
        {
            GameObject projectileRight = Instantiate(projectilePrefab, projectileMountRight.position, transform.rotation);
            NetworkServer.Spawn(projectileRight);
            GameObject projectileLeft = Instantiate(projectilePrefab, projectileMountLeft.position, transform.rotation);
            NetworkServer.Spawn(projectileLeft);
        }

        [ServerCallback]
        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Tube")
            { }
            if (other.tag == "TubeFinish")
            {
                print("TubeFinish");
                GameObject OldParent = transform.parent.gameObject;
                PlayingStation = GameObject.Find("Pos" + PlayerNumber).transform;
                transform.parent = PlayingStation;
                OldParent.SetActive(false);
                GameManager.GetComponent<ScenarioPlayer>().ServerStage = 7;
                IsInDome = true;
            }
        }

        [ServerCallback]
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag == "Tube")
            {
                print("collision Tube hit");
                GetComponent<Rigidbody>().AddExplosionForce(100, collision.GetContact(0).point, 10);
            }
        }

        private IEnumerator DelayControls()
        {
            P4_V2_Inputs.enabled = false;
            yield return new WaitForSeconds(1);
            P4_V2_Inputs.enabled = true;
        }

        public void BackOnPod()
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            CinemachineFirstController.SetActive(true);
            FirstPCamera.SetActive(true);
            FirstPCamera.GetComponent<AudioListener>().enabled = true;

            ThierdPCamera.SetActive(false);
            CinemachineThierdController.SetActive(false);
            LimitControls = true;
        }

        public float ModularClamp(float val, float min, float max, float rangemin = -180f, float rangemax = 180f)
        {
            var modulus = Mathf.Abs(rangemax - rangemin);
            if ((val %= modulus) < 0f) val += modulus;
            return Mathf.Clamp(val + Mathf.Min(rangemin, rangemax), min, max);
        }
        float ClampAngle(float angle, float from, float to)
        {
            // accepts e.g. -80, 80
            if (angle < 0f) angle = 360 + angle;
            if (angle > 180f) return Mathf.Max(angle, 360 + from);
            return Mathf.Min(angle, to);
        }
    }

}