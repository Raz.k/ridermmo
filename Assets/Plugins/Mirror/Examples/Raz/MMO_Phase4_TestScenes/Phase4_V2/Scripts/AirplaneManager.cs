﻿using UnityEngine;
using System;
using System.Collections;

public class AirplaneManager : MonoBehaviour
{
	public static AirplaneManager instance;
	public Airplane airplane;
	public Vector3 device_default_rotation;
	public CameraControl camera_control;

	void Awake ()
	{
		instance = this;
		ResetDeviceDefaultRotation ();
	}

	void Update ()
	{
		camera_control.Update ();
	}

	public void StartCameraControl ()
	{
		camera_control.active = true;
	}

	public void StopCameraControl ()
	{
		camera_control.active = false;
	}

	public void StartAirplaneControl ()
	{
		airplane.active = true;
	}

	public void StopAirplaneControl ()
	{
		airplane.active = false;
	}

	public void ResetDeviceDefaultRotation ()
	{
		device_default_rotation = Vector3.down - Input.acceleration;
	}

	[Serializable]
	public class CameraControl
	{
		public bool active;
		public Transform camera;
		public Transform target_to_follow;
		public Transform target_to_lookat;
		public float follow_smoothness = 3.33f;
		public float lookat_smoothness = 3.33f;

		public void Update ()
		{
			if (active) {
				camera.position = Vector3.Slerp (camera.position, target_to_follow.position, follow_smoothness * Time.deltaTime);
				camera.rotation = Quaternion.Slerp (camera.rotation, Quaternion.LookRotation (target_to_lookat.position - camera.position), follow_smoothness * Time.deltaTime);
			}
		}
	}
}
