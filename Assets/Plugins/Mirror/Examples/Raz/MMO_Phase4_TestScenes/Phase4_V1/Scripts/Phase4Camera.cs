using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase4Camera : MonoBehaviour
{

    public Material SkyBoxMat;

    void Start()
    {
        RenderSettings.skybox = SkyBoxMat;
    }

    
}
