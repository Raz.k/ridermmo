using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace Mirror.Examples.MMO_SpaceShip
{
    public class Phase4Inputs : NetworkBehaviour
    {


        [Header("Movement")]
        public float rotationSpeed = 100;
        public float horizontal;
        public float vertical;
        Vector2 MovementValues;

        public bool Left;
        public bool Up;
        public bool Right;
        public bool Down;

        public float Thrust = 0;

        private void Start() { }

        public void CameraMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer)// will happen once 
            {
                if (context.performed)
                {
                    MovementValues = context.ReadValue<Vector2>();
                    horizontal = MovementValues.x;
                    if (MovementValues.x > 1) horizontal /= 20;
                    if (MovementValues.x < -1) horizontal /= 20;

                    vertical = MovementValues.y;
                    if (MovementValues.y > 1) vertical /= 20;
                    if (MovementValues.y < -1) vertical /= 20;
                }                
            }
        }


        public void PlayerLeftMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Left = true;
            if (isLocalPlayer && context.canceled) Left = false;
        }
        public void PlayerRightMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Right = true;
            if (isLocalPlayer && context.canceled) Right = false;
        }
        public void PlayerUpMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Up = true;
            if (isLocalPlayer && context.canceled) Up = false;
        }
        public void PlayerDownMovement(InputAction.CallbackContext context)
        {
            if (isLocalPlayer && context.performed) Down = true;
            if (isLocalPlayer && context.canceled) Down = false;
        }

    }

}