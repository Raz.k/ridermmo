using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera3rdPerson : MonoBehaviour
{
    public GameObject Camera;

    void Start()
    {
        // ref camera 
        Camera = transform.GetChild(0).gameObject;
    }

    public void SetActive(bool val)
    {Camera.SetActive(val);}
    
}
