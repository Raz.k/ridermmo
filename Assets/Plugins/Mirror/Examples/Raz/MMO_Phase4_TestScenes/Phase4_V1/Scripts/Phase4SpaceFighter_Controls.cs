using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class Phase4SpaceFighter_Controls : NetworkBehaviour
    {        

        [Header("Movement")]
        public float rotationSpeed = 100;
        public float horizontal;
        public float vertical;
                
        public KeyCode A_key = KeyCode.A;
        public KeyCode S_key = KeyCode.S;
        public KeyCode D_key = KeyCode.D;
        public KeyCode W_key = KeyCode.W;

        public bool A;
        public bool S;
        public bool D;
        public bool W;

        float Thrust = 0;

        private void Update()
        {
            if (isLocalPlayer)
            {
                //// User rotate 
                horizontal = Input.GetAxis("Horizontal");
                vertical = Input.GetAxis("Vertical");

                A = Input.GetKey(A_key) ? true : false;
                S = Input.GetKey(S_key) ? true : false;
                D = Input.GetKey(D_key) ? true : false;
                W = Input.GetKey(W_key) ? true : false;
            }
        }


    }

}