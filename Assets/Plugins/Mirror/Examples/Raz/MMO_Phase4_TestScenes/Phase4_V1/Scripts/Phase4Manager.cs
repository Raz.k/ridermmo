using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mirror.Examples.MMO_SpaceShip
{
    public class Phase4Manager : NetworkBehaviour
    {
        public int PlayersLocalCount;
        public int NumberOfPlayersForStartPlaying;
        public string ClientsTagName = "";
        
        bool done = false;



        // SyncVar's
        [SyncVar(hook = nameof(UpdateClientsAboutPlayers))] public int Players;
        [SyncVar(hook = nameof(UpdateClientsAboutDockStatus))] public bool IsFighters_Dock = true;

        // update every client 
        private void UpdateClientsAboutPlayers(int _old, int _new) { PlayersLocalCount = _new; }
        private void UpdateClientsAboutDockStatus(bool _old, bool _new) { StartCoroutine(DelayIt()); }

        // every client gets all his instance SpaceFighter's
        IEnumerator DelayIt()
        {
            yield return new WaitForSeconds(3);
            // find fighter and updates about docking status 
            GameObject[] sfs = GameObject.FindGameObjectsWithTag(ClientsTagName);
            foreach (var sf in sfs) { sf.GetComponent<P4_V2_SpaceFighter>().IsFollow = false; }
        }

        [Server] // will run only on server
        private void SpaceFighters_DockStatusChangedOnServer(bool NewStat) { IsFighters_Dock = NewStat; }

        void Update()
        {
            if (isServer)//only server is allowed to announce player count since he is the only that can count them
            {
                Players = NetworkServer.connections.Count;
                if (!done && NetworkServer.connections.Count == NumberOfPlayersForStartPlaying)
                { 
                    done = true;
                    IsFighters_Dock = false;                    
                }
            }           
        }

        IEnumerator RunScenario()
        {            
            yield return new WaitForSeconds(0.1f);

            //Phase4SpaceFighter.Follow(true);
            //SpaceFighters_DockStatusChangedOnServer(false);
            //yield return new WaitForSeconds(20);

            //SpaceFighter.StopFollow();            
        }
    }
}