using PathCreation.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class Phase4SpaceFighter : NetworkBehaviour
    {
        public float MovementVal = 0.02f;
        public GameObject FPCamera;
        public GameObject TheirdPCamera;
        Phase4Inputs Phase4Inputs;
        Phase4PathColor phase4PathColor;
        Transform PlayingStation;
        AudioSource audioSource;
        GameObject GameManager;
        Phase4Manager Phase4Manager;
        // for cam rotation clamp
        float rotX;
        float rotY;
        // "C" switch cameras 
        public KeyCode CameraSwitch = KeyCode.C;
        bool camBool = true;
        // colors
        Material mat;
        public GameObject Cockpit3_Body;
        public GameObject Cockpit3_Interior;
        public GameObject MainBody2;
        public GameObject TrustreGlow;

        // SyncVar's        
        [SyncVar(hook = nameof(UpdateFollowStat))] public bool IsFollow;
        private void UpdateFollowStat(bool _old, bool _new)
        {
            PlayingStation.gameObject.GetComponent<Phase4FighterPhatFollower>().Follow = _new;
            if (_new)
            {
                print("Updates all StarShips In all clients:  called from Follow()  ->  IsFollow = true; ");
                audioSource.Play();
            }
            else
            {
                print("Updates all StarShips In all clients:  called from StopFollow()  ->  IsFollow = false; ");
                audioSource.volume = 0.5f;
            }
        }
        public void Follow(bool isFollow) { IsFollow = isFollow; }

        [SyncVar(hook = nameof(UpdatePlayerNumber))] public int PlayerNumber = 0;
        private void UpdatePlayerNumber(int _old, int _new) {PlayerNumber = _new;}

        private void Awake()
        {
            GameManager = GameObject.Find("GameManager");
            Phase4Inputs = GetComponent<Phase4Inputs>();
            audioSource = GetComponent<AudioSource>();            
        }

        private void Start()
        {            
            if (isLocalPlayer)
            {
                PlayerNumber = GameManager.GetComponent<Phase4Manager>().Players;
                Phase4Inputs.enabled = true;                
            }
            // position the player on the playing station
            if (PlayerNumber == 0)            
                PlayerNumber = GameManager.GetComponent<Phase4Manager>().Players;
            
            PlayingStation = GameObject.Find("Following_Container_" + PlayerNumber).transform; 
            transform.parent = PlayingStation;
            transform.position = PlayingStation.position;            
            audioSource.enabled = true;
            
            TheirdPCamera = PlayingStation.GetComponent<Camera3rdPerson>().Camera;
            //Camera.transform.parent = transform.parent;
            if (isLocalPlayer)
            {
                PlayingStation.GetComponent<Camera3rdPerson>().SetActive(true);
                //FPCamera.SetActive(true);
            }

            // color
            phase4PathColor = PlayingStation.transform.parent.GetComponent<Phase4PathColor>();

            if (phase4PathColor.Color.ToString() == "Red")
            {
                mat = phase4PathColor.Red;
                TrustreGlow.GetComponent<TrailRenderer>().material  = mat;
                Cockpit3_Body.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
                Cockpit3_Interior.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
                MainBody2.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
            }
            if (phase4PathColor.Color.ToString() == "Blue")
            {
                mat = phase4PathColor.Blue;
                TrustreGlow.GetComponent<TrailRenderer>().material = mat;
                Cockpit3_Body.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
                Cockpit3_Interior.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
                MainBody2.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
            }
            if (phase4PathColor.Color.ToString() == "Green")
            {
                mat = phase4PathColor.Green;
                TrustreGlow.GetComponent<TrailRenderer>().material = mat;
                Cockpit3_Body.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
                Cockpit3_Interior.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
                MainBody2.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
            }

        }

        private void Update()
        {
            // movement in bounds 
            if (Phase4Inputs.Down)
                Vector3.Lerp(transform.localPosition, transform.localPosition += new Vector3(MovementVal, 0, 0), 1);
            if (Phase4Inputs.Up)
                Vector3.Lerp(transform.localPosition, transform.localPosition += new Vector3(-MovementVal, 0, 0), 1);
            if (Phase4Inputs.Right)
                Vector3.Lerp(transform.localPosition, transform.localPosition += new Vector3(0, MovementVal, 0), 1);
            if (Phase4Inputs.Left)
                Vector3.Lerp(transform.localPosition, transform.localPosition += new Vector3(0, -MovementVal, 0), 1);

            // move            
            //transform.position += transform.forward * Phase4Inputs.Thrust * Time.deltaTime;

            // camera rotation 

            rotX += Phase4Inputs.vertical * Phase4Inputs.rotationSpeed * Time.deltaTime;
            rotX = Mathf.Clamp(rotX, -15f, 15f);
            rotY += Phase4Inputs.horizontal * Phase4Inputs.rotationSpeed * Time.deltaTime;
            rotY = Mathf.Clamp(rotY, -15f, 15f);             
            FPCamera.transform.localRotation = Quaternion.Euler(rotX, rotY, 0f);
            TheirdPCamera.transform.localRotation = Quaternion.Euler(-rotY, rotX, 90f);

            // camera shack
            float x = Random.Range(-0.01f, 0.01f) * 0.05f;
            float y = Random.Range(-0.01f, 0.01f) * 0.05f;
            FPCamera.transform.localPosition = new Vector3(x, y, 0);

            if (isLocalPlayer)
            {
                if (Input.GetKeyDown(CameraSwitch))
                {
                    FPCamera.SetActive(camBool);
                    PlayingStation.GetComponent<Camera3rdPerson>().SetActive(!camBool);
                    camBool = !camBool;
                }
            }

        }

        private void LateUpdate()
        {
            // limit movement to bounds              
            Vector3 currentPose = transform.localPosition;
            currentPose.x = Mathf.Clamp(currentPose.x, -5f, 5f);
            currentPose.y = Mathf.Clamp(currentPose.y, -5f, 5f);
            transform.localPosition = currentPose;            
        }


        [Command]
        public void CmdFire()
        {
            print("fire");
        }
    }

}
