using UnityEngine;
using MMO;
using System;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class SpaceFighter : NetworkBehaviour
    {
        SpaceFighter_Controls spaceFighter_Controls;        
        GameObject MMO_Manager;
        GameObject PlayingStation;
        GameObject StarShip;
        AudioSource audioSource;

        
        public int Status = 0;
        public GameObject Camera;
        public GameObject SpawnPoint;

        [Header("Projectile")]
        public GameObject projectilePrefab;
        public Transform projectileMountRight;
        public Transform projectileMountLeft;
        public Transform projectileMountCenter;
        
        [Header("SpaceFighter_Parts")]
        public GameObject BlasterRight;
        public GameObject BlasterCenter;
        public GameObject BlasterLeft;
        public GameObject Engine;
        public GameObject WingRight;
        public GameObject WingLeft;
        public GameObject Body;
        public GameObject TrustreGlow;


        // SyncVar's
        [SyncVar] public bool IsGhostPlayer = false;
        [SyncVar(hook = nameof(SpaceFighterDockStatusChanged))] public bool IsSpaceFighter_Dock = true;
                  
        void SpaceFighterDockStatusChanged(bool _, bool NewStat){SpaceFighter_PartsDisplay(true);}
        public void SpaceFighter_PartsDisplay(bool Show)
        {
            BlasterRight.SetActive(Show);
            BlasterLeft.SetActive(Show);
            Engine.SetActive(Show);
            WingRight.SetActive(Show);
            WingLeft.SetActive(Show);
            Body.SetActive(Show);
            TrustreGlow.SetActive(Show);

            BlasterCenter.SetActive(!Show);
        }


        private void Awake()
        {
            spaceFighter_Controls = GetComponent<SpaceFighter_Controls>();
            MMO_Manager = GameObject.Find("MMO_Manager");
            StarShip = GameObject.Find("StarShip Container");
            audioSource = GetComponent<AudioSource>();
            SpaceFighter_PartsDisplay(false);
        }       
        private void Start()
        {
            #region bypass
            // TODO:  this is a bypass !!  for Demo purposes only
            RFID_Reader_Call call1 = new RFID_Reader_Call();
            call1.playerRFID = "someCode3";
            call1.ReaderNumber = 1;
            MMO_Events.onRFID_Submit?.Invoke(call1);
            #endregion

            // position the player on the playing station 
            PlayingStation = GameObject.Find("PS" + MMO_Manager.GetComponent<Players>().ConnectedPlayers.Count);
            transform.position = PlayingStation.transform.position;
            transform.rotation = PlayingStation.transform.rotation;

            if (isLocalPlayer)
            {
                spaceFighter_Controls.enabled = true;
                Camera.SetActive(true);
            }
        }

        void Update()
        {
            if (StarShip.GetComponent<StarShip>().IsFollow && IsSpaceFighter_Dock)
            {
                transform.position = PlayingStation.transform.position;
                transform.rotation = PlayingStation.transform.rotation;
                audioSource.enabled = false;

                BlasterCenter.transform.Rotate(spaceFighter_Controls.vertical * spaceFighter_Controls.rotationSpeed / 10 * Time.deltaTime
                                             , spaceFighter_Controls.horizontal * spaceFighter_Controls.rotationSpeed / 10 * Time.deltaTime, 0);
            }
            else
            {// movement for local player
                if (!IsSpaceFighter_Dock && isLocalPlayer)
                {
                    //TODO: move to event                    
                    GetComponent<BoxCollider>().enabled = true;
                    transform.parent = null;
                    //sound
                    audioSource.enabled = true;
                    // rotate            
                    transform.Rotate(spaceFighter_Controls.vertical * spaceFighter_Controls.rotationSpeed * Time.deltaTime, spaceFighter_Controls.horizontal * spaceFighter_Controls.rotationSpeed * Time.deltaTime, 0);
                    // move            
                    transform.position += transform.forward * spaceFighter_Controls.Thrust * Time.deltaTime;
                }
            }
        }

        // Client calls 
        [Command]
        public void CmdFire()
        {
            if (StarShip.GetComponent<StarShip>().IsFollow && IsSpaceFighter_Dock)
            {
                GameObject projectileRight = Instantiate(projectilePrefab, projectileMountCenter.transform.position, projectileMountCenter.transform.rotation);
                NetworkServer.Spawn(projectileRight);
            }
            if (!IsSpaceFighter_Dock)
            {
                GameObject projectileRight = Instantiate(projectilePrefab, projectileMountRight.position, transform.rotation);
                NetworkServer.Spawn(projectileRight);
                GameObject projectileLeft = Instantiate(projectilePrefab, projectileMountLeft.position, transform.rotation);
                NetworkServer.Spawn(projectileLeft);
                //RpcOnFire();
                //print("fire!");
            }
        }
        
        [Command]
        private void SpaceFighters_DockStatusChangedOnServer(bool NewStat) { IsSpaceFighter_Dock = NewStat; }
       
    }
}
