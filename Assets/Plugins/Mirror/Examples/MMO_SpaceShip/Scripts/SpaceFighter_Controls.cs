﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{

    public class SpaceFighter_Controls : NetworkBehaviour
    {
        SpaceFighter spaceFighter;


        [Header("Movement")]
        public float rotationSpeed = 100;
        public float horizontal;
        public float vertical;
        public Vector3 forward;
        public float Thrust;
        public KeyCode ThrustKey = KeyCode.RightControl;
        

        [Header("Firing")]
        public KeyCode shootKey = KeyCode.Space;


        void Awake()
        {
            spaceFighter = GetComponent<SpaceFighter>();
        }


        private void Update()
        {
            if (isLocalPlayer)
            {
                // User rotate 
                horizontal = Input.GetAxis("Horizontal");
                vertical = Input.GetAxis("Vertical");

                // User move 
                if (Input.GetKeyDown(ThrustKey)) Thrust = 30;
                if (Input.GetKeyUp(ThrustKey)) Thrust = 0.001f;
                // User fire
                if (Input.GetKeyDown(shootKey)) spaceFighter.CmdFire();
            }
        }

    }
}