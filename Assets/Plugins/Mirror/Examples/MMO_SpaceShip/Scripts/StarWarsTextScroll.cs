using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class StarWarsTextScroll : MonoBehaviour
    {

        public float ScrollSpeed = 20;

        private void Update()
        {
            Vector3 pos = transform.position;            
            pos -= Vector3.up * ScrollSpeed * Time.deltaTime;
            transform.position = pos;
        }
        
    }
}
