using System.Collections.Generic;
using UnityEngine;
using PathCreation.Examples;
using PathCreation;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class StarShip : NetworkBehaviour
    {        
        PathShipFollower pathShipFollower;        
        AudioSource audioSource;
        Animator animator;

        private void Start()
        {
            pathShipFollower = GetComponent<PathShipFollower>();
            audioSource = GetComponent<AudioSource>();
            animator = GetComponentInChildren<Animator>();
        }

        // SyncVar's
        [SyncVar(hook = nameof(ShipSpeedChanged))] public float ShipSpeed;
        [SyncVar(hook = nameof(UpdateFollowStat))] public bool IsFollow;                
        private void UpdateFollowStat(bool _old, bool _new)
        {        
            pathShipFollower.Follow = _new;
            if (_new)
            {
                print("Updates all StarShips In all clients:  called from Follow()  ->  IsFollow = true; ");
                audioSource.Play();
                animator.enabled = true;                
            }
            else
            {
                print("Updates all StarShips In all clients:  called from StopFollow()  ->  IsFollow = false; ");
                audioSource.volume = 0.5f;
            }
        }
        private void ShipSpeedChanged(float _old, float _new)
        {
            ShipSpeed = _new;
            pathShipFollower.speed = _new;            
        }
        // OutSide Calls 
        public void ShipSpeedChange(float speed) {ShipSpeed = speed;}
        public void Follow(){IsFollow = true;}
        public void StopFollow(){IsFollow = false;}
    }    
}