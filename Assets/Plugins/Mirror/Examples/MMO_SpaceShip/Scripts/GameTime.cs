using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class GameTime : NetworkBehaviour
    {
        public float PingFrequency;
        public float PingWindowSize;
        public float LocalTime;
        public float ServerTime;
        public float DiffrenceClientServer;
        public float roundTripTime;
        public float rttStandardDeviation;
        public float rttVariance;
        public float timeStandardDeviation;
        public float timeVariance;

        void Start(){}
        
        void Update()
        {
            PingFrequency = NetworkTime.PingFrequency;
            PingWindowSize = NetworkTime.PingWindowSize;
            LocalTime = (float)NetworkTime.localTime;
            ServerTime = (float)NetworkTime.time;
            DiffrenceClientServer = (float)NetworkTime.offset;
            roundTripTime = (float)NetworkTime.rtt;
            rttStandardDeviation = (float)NetworkTime.rttStandardDeviation;
            rttVariance = (float)NetworkTime.rttVariance;
            timeStandardDeviation = (float)NetworkTime.timeStandardDeviation;
            timeVariance = (float)NetworkTime.timeVariance;
        }
    }
}