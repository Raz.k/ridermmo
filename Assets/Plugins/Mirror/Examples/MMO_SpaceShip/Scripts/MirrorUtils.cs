﻿using MMO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class MirrorUtils : NetworkBehaviour
    {
        public int PlayersLocalCount;
        public int PlayersForStartPlaying;        
        public StarShip StarShip;
        public GameObject StarFieldEffect;
        bool done = false;

        // SyncVar's
        [SyncVar(hook = nameof(UpdateClientsAboutPlayers))] public int Players;
        [SyncVar(hook = nameof(UpdateClientsAboutDockStatus))] public bool IsFighters_Dock = true;  
        
        // update every client 
        private void UpdateClientsAboutPlayers(int _old, int _new) {PlayersLocalCount = _new;}
        private void UpdateClientsAboutDockStatus(bool _old, bool _new) {StartCoroutine(DelayIt());}

        // every client gets all his instance SpaceFighter's  
        IEnumerator DelayIt()
        {
            yield return new WaitForSeconds(1);
            // find fighter and updates about docking status 
            GameObject[] sfs = GameObject.FindGameObjectsWithTag("SpaceFighter");
            foreach (var sf in sfs) {sf.GetComponent<SpaceFighter>().IsSpaceFighter_Dock = false;}
        }

        [Server] // will run only on server
        private void SpaceFighters_DockStatusChangedOnServer(bool NewStat) {IsFighters_Dock = NewStat;}

        void Update()
        {            
            if (isServer)//only server is allowed to announce player count since he is the only that can count them
            {
                Players = NetworkServer.connections.Count;
                if (!done && NetworkServer.connections.Count == PlayersForStartPlaying)
                {done = true;StartCoroutine(SomeTime());}
            }            
        }

        IEnumerator SomeTime()
        {
            print("Game Scenario  ->  called and runs only on server , updates all players");
            // warp speed
            yield return new WaitForSeconds(6);
            
            // out of warp speed earth insight
            // shotting docked to SpaceShip
            StarShip.Follow();
            StarShip.ShipSpeedChange(0.5f);
            yield return new WaitForSeconds(20);

            // shotting and flying alone in SpaceFighter
            StarShip.StopFollow();
            SpaceFighters_DockStatusChangedOnServer(false);
        }

                     
    }
}