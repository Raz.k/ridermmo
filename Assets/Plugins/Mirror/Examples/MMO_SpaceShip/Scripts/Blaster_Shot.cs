using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.MMO_SpaceShip
{
    public class Blaster_Shot : NetworkBehaviour
    {
        public float destroyAfter = 1;
        public Rigidbody rigidBody;
        public float force = 1000;
        public GameObject explosionPrefab;
        private AudioSource audioSource;

        public override void OnStartServer()
        {
            Invoke(nameof(DestroySelf), destroyAfter);
        }

        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();            
        }

        // set velocity for server and client. this way we don't have to sync the
        // position, because both the server and the client simulate it.
        void Start()
        {
            audioSource.Play();
            rigidBody.AddForce(transform.forward * force);
        }

        // destroy for everyone on the server
        [Server]
        void DestroySelf()
        {
            NetworkServer.Destroy(gameObject);
        }

        // ServerCallback because we don't want a warning if OnTriggerEnter is
        // called on the client
        [ServerCallback]
        void OnTriggerEnter(Collider co)
        {
            
            if ( co.name != "Blaster_Shot(Clone)") //co.GetComponent<Rigidbody>() != null &&
            {
                //print("HIT!! " + co.name);

                // add physic force at explosion point
                //co.gameObject.GetComponent<Rigidbody>().AddExplosionForce(10, transform.transform.position, 1000);

                // simulate explosion
                GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
                NetworkServer.Spawn(explosion);
                NetworkServer.Destroy(gameObject);
            }
        }
    }
}