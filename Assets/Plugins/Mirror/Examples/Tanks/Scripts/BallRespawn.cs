using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRespawn : MonoBehaviour
{
    public Vector3 RespawnPosition;
        
    void Update()
    {        
        if (transform.position.y < 0) 
        {            
            transform.position = RespawnPosition;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }   
    }
}
