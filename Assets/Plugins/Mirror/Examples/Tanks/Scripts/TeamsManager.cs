using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Mirror.Examples.Tanks
{

    public class TeamsManager : NetworkBehaviour
    {
        [SyncVar]//(hook = nameof(UpdateClientsAboutRed))]
        public int Players;

        void Update()
        {            
            //only server is allowed to announce player count since he is the only that can count them
            if (isServer)
            {
                Players = NetworkServer.connections.Count;
            }                           
        }
    }

}