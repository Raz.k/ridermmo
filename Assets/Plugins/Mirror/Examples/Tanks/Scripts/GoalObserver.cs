using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mirror.Examples.Tanks
{

    public class GoalObserver : NetworkBehaviour
    {
        [SerializeField] public AudioSource AudioSource;
        [SerializeField] public AudioClip Audio;


        [SerializeField] public Text RedScoreText;
        [SerializeField] public Text BlueScoreText;

        [SyncVar(hook = nameof(UpdateClientsAboutRed))]
        public int RedScore;        
        [SyncVar(hook = nameof(UpdateClientsAboutBlue))]
        public int BlueScore;

        private void Start() { RedScore = BlueScore = 0; }

        
        
        private void UpdateClientsAboutRed(int oldScore , int newScore)
        {
            RedScore = newScore;
            RedScoreText.text = RedScore.ToString();
            PlaySound(Audio);
        }
        private void UpdateClientsAboutBlue(int oldScore, int newScore)
        {
            BlueScore = newScore;
            BlueScoreText.text = BlueScore.ToString();
            PlaySound(Audio);
        }


        public void PlaySound(AudioClip clip)
        {
            AudioSource.clip = clip;            
            AudioSource.Play();
        }


    }


}
