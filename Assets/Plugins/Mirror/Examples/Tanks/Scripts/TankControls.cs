//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



namespace Mirror.Examples.Tanks
{
    public class TankControls : NetworkBehaviour
    {
        Tank tank;
        bool IsCoroutineRunning;
        [SyncVar]
        public bool AI = false;
        public float Factor = 0.5f;

        [Header("Movement")]
        public float rotationSpeed = 100;
        public float horizontal;
        public float vertical;
        public Vector3 forward;

        [Header("Firing")]
        public KeyCode shootKey = KeyCode.Space;
        

        void Awake()
        {
            tank = GetComponent<Tank>();
            Factor = Random.Range(-1f, 1f);
            // adding an AI Button to toggle AI or player mode
            GameObject.Find("AI_Button").GetComponent<Button>().onClick.AddListener(ToggleAI);
        }

        private void Update()
        {
            if (AI)
            {
                if (!IsCoroutineRunning)
                    StartCoroutine(RandomControls());

                horizontal = Mathf.Lerp(-1, 1, Factor); 
                vertical =  Mathf.Lerp(-1, 1, Factor);
                forward = transform.TransformDirection(Vector3.forward);
            }
            else
            {
                // User rotate 
                horizontal = Input.GetAxis("Horizontal") * 2f;
                // User move 
                vertical = Input.GetAxis("Vertical");
                forward = transform.TransformDirection(Vector3.forward * 2f);
                // User fire
                if (Input.GetKeyDown(shootKey))                
                    tank.CmdFire();                
            }
        }




        IEnumerator RandomControls()
        {
            IsCoroutineRunning = true;

            // firing every 1 sec
            tank.CmdFire();
            yield return new WaitForSeconds(1f);

            IsCoroutineRunning = false;
        }



        //[Client]// runs only on this client
        //public void AIfromClient()
        //{
        //    ToggleAI();
        //    print("Client");
        //}

        [Command]
        public void ToggleAI()
        {
            AI = !AI;
            print("Command");
        }

    }
}
