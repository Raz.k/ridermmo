using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.Tanks
{

    public class TankTeamColor : MonoBehaviour
    {
        public GameObject Sphere;

        void Start()
        {
            if (GetComponent<Tank>().Team == Util.Team.Red)
                Sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            else
                Sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
        }



    }
}