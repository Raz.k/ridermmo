using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Mirror.Examples.Tanks
{

    public class Goal : NetworkBehaviour
    {
        public GoalObserver Observer;

        [ServerCallback]
        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Ball"))
            {
                // red or blue ?
                if (transform.tag == "Red")
                    Observer.BlueScore++;
                if (transform.tag == "Blue")
                    Observer.RedScore++;
            }
        }

    }

}