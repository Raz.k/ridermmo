using UnityEngine;
using UnityEngine.AI;

namespace Mirror.Examples.Tanks
{
    [RequireComponent(typeof(TankControls))]
    public class Tank : NetworkBehaviour
    {
        [Header("Components")]
        public NavMeshAgent agent;
        public Animator animator;
        
        [Header("Projectile")]        
        public GameObject projectilePrefab;
        public Transform projectileMount;

        [Header("User")]
        [SyncVar]
        public string playerName;
        [SyncVar]
        public Util.Team Team;

        TankControls tankControls;
        
        
        void Awake()
        {
            tankControls = GetComponent<TankControls>();
            TeamsManager t = GameObject.Find("GameManager").GetComponent<TeamsManager>();
            t.Players++;
            if (t.Players % 2 == 1)
                Team = Util.Team.Red;
            else
                Team = Util.Team.Blue;
        }

        void Update()
        {
            // movement for local player
            if (!isLocalPlayer) return;

            #region Tank Controls
            
            // rotate            
            transform.Rotate(0, tankControls.horizontal * tankControls.rotationSpeed * Time.deltaTime, 0);
            // move            
            agent.velocity = tankControls.forward * Mathf.Max(tankControls.vertical, 0) * agent.speed;
            animator.SetBool("Moving", agent.velocity != Vector3.zero);
        
            #endregion Tank Controls
        }

        // this is called on the server
        [Command]
        public void CmdFire()
        {
            GameObject projectile = Instantiate(projectilePrefab, projectileMount.position, transform.rotation);
            NetworkServer.Spawn(projectile);
            RpcOnFire();
        }

        // this is called on the tank that fired for all observers
        [ClientRpc]
        void RpcOnFire()
        {
            animator.SetTrigger("Shoot");
        }




        
        //private void OnCollisionEnter(Collision collision)
        //{
        //    func();
        //}

        //[ClientRpc]
        //void func()
        //{
        //    print("hit");
        //}




        //private void oncollisionenter(collision collision)
        //{
        //    //if (collision.gameobject.tag.contains("projectile"))
        //    //{
        //    //    print("hit");
        //    //}
        //}

        //private void OnTriggerEnter(Collider other)
        //{
        //    if (other.gameObject.tag.Contains("Projectile"))
        //    {
        //        print("hit");
        //    }
        //}

    }
}
