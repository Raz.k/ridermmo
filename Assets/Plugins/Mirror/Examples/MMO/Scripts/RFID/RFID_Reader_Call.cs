﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace MMO
{
    [Serializable]
    public class RFID_Reader_Call
    {
        public string playerRFID;
        public int ReaderNumber;
    }
}