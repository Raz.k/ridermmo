﻿using System;
using UnityEngine;

namespace MMO
{
[Serializable]
public class RFID_Reader
{
    public bool IsActive;
    public int ReaderNumber;
    public string ReaderID;    
    public Transform SpawnPoint;
}
}