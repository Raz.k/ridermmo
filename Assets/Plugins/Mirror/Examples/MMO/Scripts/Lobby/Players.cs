using System;
using System.Collections.Generic;
using UnityEngine;


namespace MMO
{

    // purpose - manage players to/in/from MMO  
    public class Players : MonoBehaviour
    {
        public List<Transform> PlayingStations;
        public List<MMO_Player> ConnectedPlayers;

        private int PlayerCounter = 0;

        private void OnEnable()
        {
            MMO_Events.onRFID_Submit += PlayerRFID_Submit;
        }
        private void OnDisable()
        {
            MMO_Events.onRFID_Submit -= PlayerRFID_Submit;
        }




        // receive RFID from User
        private void PlayerRFID_Submit(RFID_Reader_Call call)
        {
            MMO_Player NewMMO_Player = new MMO_Player();
            NewMMO_Player.Number = ++PlayerCounter;
            NewMMO_Player.playerRFID = call.playerRFID;
            NewMMO_Player.ReaderNumber = call.ReaderNumber;

            //TODO:  call API to populate user data 

            NewMMO_Player.playerId = "1";
            NewMMO_Player.IsPopulated = true;
            NewMMO_Player.playerLoc = "dd";
            NewMMO_Player.playerNick = "q1";
            NewMMO_Player.SpawnPoint = transform;

            // add new player 
            ConnectedPlayers.Add(NewMMO_Player);
        }

    }
}