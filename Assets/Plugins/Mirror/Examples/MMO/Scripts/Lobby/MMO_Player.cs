using System;
using UnityEngine;

[Serializable]
public class MMO_Player 
{
    // player connection data
    public int Number;
    public string playerRFID;
    public int ReaderNumber;
    public Transform SpawnPoint;

    // API to populate
    public bool IsPopulated = false;
    public string playerId;
    public string playerLoc;
    public string playerNick;    
}
