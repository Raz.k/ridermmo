using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace MMO
{
    public class MMO_Manager : MonoBehaviour
    {

        //[Scene]                
        //public string SceneToLoad = ""; 
        private Players players;
        public Material SkyBoxMat; 

        private void Awake()
        {
            RenderSettings.skybox = SkyBoxMat;
            if (!SceneManager.GetSceneByName("SpaceShip").isLoaded )            
               SceneManager.LoadScene("SpaceShip", LoadSceneMode.Additive);
            //if (SceneManager.GetSceneByName(SceneToLoad).isLoaded)
              //  SceneManager.LoadScene(SceneToLoad, LoadSceneMode.Additive);
        }
   

        private void Start()
        {
            players = GetComponent<Players>();            
        }
       
    }

}