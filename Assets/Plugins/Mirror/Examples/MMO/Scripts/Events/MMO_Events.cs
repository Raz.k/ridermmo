﻿using System;

namespace MMO
{
    public static class MMO_Events
    {
        public static Action<RFID_Reader_Call> onRFID_Submit;
        public static Action<bool> onSpaceFightersDockStatChanged;
    }
}