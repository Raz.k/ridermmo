﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    public bool LocalRotation;
    public float rotationSpeedY;
    public float rotationSpeedZ;
    private Vector3 vec;

    private void Update()
    {
        vec = new Vector3(0,   rotationSpeedY * Time.deltaTime, rotationSpeedZ * Time.deltaTime);
        if (LocalRotation)        
            transform.localRotation = Quaternion.Euler(vec);
        else
            transform.Rotate(vec);        
    }
}
