﻿using UnityEngine;

public class Wiggle3 : MonoBehaviour
{
    // "Bobbing" animation from 1D Perlin noise.

    // Range over which height varies.

    public float Max_X, Max_Y, Max_Z = 2.0f;
    private float x0, y0, z0, xMax, yMax, zMax;

    // Distance covered per second along X axis of Perlin plane.
    public float ScaleSpeed = 1.0f;
    private bool AnimatedWiggle = false;
    

    void Awake()
    {
        x0 = transform.localPosition.x;
        y0 = transform.localPosition.y;
        z0 = transform.localPosition.z;

        xMax = Random.Range(Max_X / 2, Max_X);
        yMax = Random.Range(Max_Y / 2, Max_Y);
        zMax = Random.Range(Max_Z / 2, Max_Z);

        //widhtScale = Random.Range(widhtScale / 2, widhtScale);
        //heightScale = Random.Range(heightScale/2, heightScale);
        ScaleSpeed = Random.Range(ScaleSpeed / 2, ScaleSpeed);
        
    }

    public float Remap(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue)
    {

        float OldRange = (OldMax - OldMin);
        float NewRange = (NewMax - NewMin);
        float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;

        return (NewValue);
    }



    void Update()
    {
        if (xMax == 0)
            AnimatedWiggle = true;
        if (AnimatedWiggle)
        {
            xMax = Max_X;
            yMax = Max_Y;
            zMax = Max_Z;

        }

        float noise = Mathf.PerlinNoise(Time.time * 1/ ScaleSpeed, Time.time * 1/ScaleSpeed);

        float width = xMax * Remap(0, 1, -1, 1, noise);
        float height = yMax * Remap(0, 1, -1, 1, noise);
        float depth = zMax * Remap(0, 1, -1, 1, noise);
        Vector3 pos = transform.localPosition;

        float x = x0 + width;
        float y = y0 + height;
        float z = z0 + depth;


        pos.x = x;// Mathf.Clamp(x, x0, x+Max_X);
        pos.y = Mathf.Clamp(y, y0, y+Max_Y);
        pos.z = z;// Mathf.Clamp(z, x0, z+Max_Z);


        transform.localPosition = pos;
    }
}