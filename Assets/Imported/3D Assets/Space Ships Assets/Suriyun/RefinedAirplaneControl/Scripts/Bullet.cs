﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{

    public float speed;
    public float life_time;
    Transform bullet;
    public Rigidbody r_body;

    public virtual void Init(Airplane airplane)
    {
        this.bullet = transform;
        r_body = GetComponent<Rigidbody>();

        r_body.AddForce(bullet.forward * speed, ForceMode.VelocityChange);
    }

    public virtual void Update()
    {
        life_time -= Time.deltaTime;

        if (life_time < 0)
        {
            Destroy(this.gameObject);
        }
    }

}
