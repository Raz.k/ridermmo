﻿using UnityEngine;
using System;
using System.Collections;

public class Airplane : MonoBehaviour
{

	public bool active;
	public float thrust = 11f;
	public float booster_thrust = 6f;
	public float turn_rate = 60f;
	public float agility = 6f;
    public float tilt_power = 1f;
	public bool booster_on = false;
	public float velocity;
	public Weapon weapon;
	public UnderTheHood under_the_hood;

	protected virtual void Start ()
	{
		weapon.Init (this);
		under_the_hood = new UnderTheHood (this);
	}

	protected virtual void  Update ()
	{

		if (active)
			under_the_hood.Update ();

	}

	public float GetAxisX ()
	{
		return -under_the_hood.GetAccel ().x;
	}

	public float GetAxisY ()
	{
		return under_the_hood.GetAccel ().z;
	}

	[Serializable]
	public class Weapon
	{
		Airplane airplane;
		public GameObject bullet_prefab;
		public Transform[] ports;

		public void Init (Airplane airplane)
		{
			this.airplane = airplane;
		}

		public void Fire ()
		{
			foreach (Transform port in ports) {
				GameObject bullet = Instantiate (bullet_prefab, port.position, port.rotation) as GameObject;
				bullet.GetComponent<Bullet> ().Init (airplane);
			}
		}
	}
	
	[Serializable]
	public class UnderTheHood
	{
		Airplane airplane;
		Transform navigator;
		public Vector3 normal;
		public Vector3 rotation_euler;

		public UnderTheHood (Airplane airplane)
		{
			this.airplane = airplane;
			this.navigator = airplane.transform;
		}

		public void Update ()
		{
			normal = Vector3.Lerp (normal, -GetAccel (), airplane.agility * Time.deltaTime);
			rotation_euler += Vector3.up * (GetAccel ().x) * airplane.turn_rate * Time.deltaTime;

			navigator.up = normal;
			navigator.rotation = Quaternion.Euler (navigator.rotation.eulerAngles - rotation_euler);
			if (!airplane.booster_on) {
				airplane.velocity = Mathf.Lerp (airplane.velocity, airplane.thrust, 6f * Time.deltaTime);
			} else {
				airplane.velocity = Mathf.Lerp (airplane.velocity, airplane.thrust + airplane.booster_thrust, 6f * Time.deltaTime);
			}
			navigator.transform.position += navigator.forward * airplane.velocity * Time.deltaTime;
		}

		public Vector3 GetAccel ()
		{
			Vector3 re;// = Input.acceleration + AirplaneManager.instance.device_default_rotation;

			#if UNITY_EDITOR
			re.x = Input.GetAxis("Horizontal");
			re.y = -Input.GetAxis("Vertical")-1f;
			re.z = -Input.GetAxis("Vertical");
			re = re.normalized;
			#endif
            re.x = -re.x * airplane.tilt_power;

			return re;
		}

	}
}
