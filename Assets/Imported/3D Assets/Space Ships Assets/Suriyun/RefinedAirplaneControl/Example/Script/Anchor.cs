﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class Anchor : MonoBehaviour {

    public enum AnchorPostion {
        Top ,
        TopLeft ,
        TopRight ,
        Mid ,
        MidLeft ,
        MidRight ,
        Bot ,
        BotLeft ,
        BotRight
    }

    public AnchorPostion anchorPosition;
    public float offsetX;
    public float offsetY;
    public int layer = 1;

    public Camera anchor_camera;

    private float x;
    private float y;

    void Start() {
        if(!anchor_camera) {
            anchor_camera = Camera.main;
        }
        StartCoroutine(UpdateAnchor());
    }

    IEnumerator UpdateAnchor() {
        this.UpdateAnchorPosition();
        while(true) {
            transform.position = anchor_camera.ScreenToWorldPoint(new Vector3((x + offsetX) / 100f * Screen.width , (y + offsetY) / 100f * Screen.height , layer));
            yield return 0;
        }
    }

    public void UpdateAnchorPosition() {
        switch(anchorPosition) {
            case AnchorPostion.Top:
                x = 50;
                y = 100;
                break;
            case AnchorPostion.TopLeft:
                x = 0;
                y = 100;
                break;
            case AnchorPostion.TopRight:
                x = 100;
                y = 100;
                break;
            case AnchorPostion.Mid:
                x = 50;
                y = 50;
                break;
            case AnchorPostion.MidLeft:
                x = 0;
                y = 50;
                break;
            case AnchorPostion.MidRight:
                x = 100;
                y = 50;
                break;
            case AnchorPostion.Bot:
                x = 50;
                y = 0;
                break;
            case AnchorPostion.BotLeft:
                x = 0;
                y = 0;
                break;
            case AnchorPostion.BotRight:
                x = 100;
                y = 0;
                break;
        }
    }

}
