﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BtnReset : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        AirplaneManager.instance.ResetDeviceDefaultRotation();
    }
}
