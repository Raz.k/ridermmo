﻿using UnityEngine;
using System.Collections;

public class Example : MonoBehaviour {
	
	void Start () {
		// Start controlling the airplane //
		AirplaneManager.instance.StartAirplaneControl();
		// Start controlling the camera //
		AirplaneManager.instance.StartCameraControl();
	}
	
}
