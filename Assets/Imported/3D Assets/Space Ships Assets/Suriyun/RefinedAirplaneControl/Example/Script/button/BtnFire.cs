﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BtnFire : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Airplane airplane;
    SpriteRenderer ren;
    bool firing = false;

    void Start()
    {
        StartCoroutine(Firing());
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        firing = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        firing = false;
    }

    IEnumerator Firing()
    {
        while (true)
        {
            if (firing && airplane.active)
            {
                airplane.weapon.Fire();
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
