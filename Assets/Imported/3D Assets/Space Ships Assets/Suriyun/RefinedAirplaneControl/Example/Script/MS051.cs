﻿using UnityEngine;
using System.Collections;

public class MS051 : Airplane
{
	public Animator mecanim;

	protected override void Start ()
	{
		base.Start ();
	}

	float lerp_speed = 16;
	float lerped_axis_x;
	float lerped_axis_y;

	protected override void Update ()
	{
		base.Update ();

		// Lerp inputs for smooth value //
		lerped_axis_x = Mathf.Lerp(lerped_axis_x,GetAxisX(),lerp_speed * Time.deltaTime);
		lerped_axis_y = Mathf.Lerp(lerped_axis_y,GetAxisY(),lerp_speed * Time.deltaTime);

		// Update mecanim parameters //
		mecanim.SetFloat ("Steer", lerped_axis_x);
		mecanim.SetFloat ("Tilt",lerped_axis_y);

	}
}
