﻿using UnityEngine;
using System.Collections;

public class DeflectBullet : MonoBehaviour
{
	void OnCollisionEnter(Collision col){
		if (col.collider.GetComponent<Bullet> () != null) {
			Bullet bull = col.collider.GetComponent<Bullet>();
			bull.r_body.AddForce(col.contacts[0].normal * bull.speed,ForceMode.VelocityChange);
		}
	}
}
