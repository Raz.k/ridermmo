﻿using UnityEngine;
using System.Collections;

public class DieOnHit : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if(col.GetComponent<Airplane>()!=null){
			AirplaneManager.instance.StopAirplaneControl();
			AirplaneManager.instance.StopCameraControl();
		}

	}
}
