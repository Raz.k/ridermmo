﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class BtnJet : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Airplane airplane;
    float normal_speed;

    public void OnPointerDown(PointerEventData eventData)
    {
        airplane.booster_on = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        airplane.booster_on = false;
    }

}
